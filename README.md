# Flora Blog

Blog application about plants.

## Getting Started

### Prerequisites

To run this application you need to have:
* [Visual Studio](https://visualstudio.microsoft.com/pl/) with 'ASP.NET and web development' package
* [Node.js](https://nodejs.org/en/)
* [Angular](https://angular.io/)
* [MongoDB](https://www.mongodb.com/)

### Installing

1. Install [Visual Studio](https://visualstudio.microsoft.com/pl/thank-you-downloading-visual-studio/?sku=Community&rel=16#) with 'ASP.NET and web development' package.

2. Install [MongoDB](https://fastdl.mongodb.org/windows/mongodb-windows-x86_64-4.4.2-signed.msi)

3. Install [Node.js](https://nodejs.org/dist/v14.15.1/node-v14.15.1-x64.msi).

4. Use **npm** to install Angular (in Windows Command Line).

```
npm install -g @angular/cli
```

5. Go to _..\flora-blog\FloraBlog\ClientApp_, open **cmd** in this location and run 

```
npm install
```

6. Run MongoDB in cmd:

```
mongod
```

7. Open **sln** file, build the solution and run.

## Used technology

* [Angular](https://angular.io/) - Used for client side
* [ASP.NET](https://dotnet.microsoft.com/apps/aspnet) - Used for server side
* [Bootstrap](https://getbootstrap.com/) - Used to create responsive web pages
* [CKEditor](https://ckeditor.com/) - Used to create advanced data editor
* [Angular Material](https://material.angular.io/) - Used to create some of web pages elements
* [MongoDB](https://www.mongodb.com/) - Used for database

## Authors

* **Amadeusz Kornecki** - [Mozzarto](https://gitlab.com/Mozzarto)
* **Monika Kapuśniak** - [mokapusnia](https://gitlab.com/mokapusnia)
* **Konrad Bober** - [konradbober1995](https://gitlab.com/konradbober1995)

See also the list of [project members](https://gitlab.com/Mozzarto/flora-blog/-/project_members) who participated in this project.
