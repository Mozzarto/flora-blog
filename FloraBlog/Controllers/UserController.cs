﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using FloraBlog.Models;
using FloraBlog.Services;

namespace FloraBlog.Controllers
{
    [Route("api/users")]
    public class UserController : Controller
    {
        private readonly UserService UserService;
        private readonly PostService PostService;
        private readonly CommentService CommentService;

        public UserController(UserService userService, PostService postService, CommentService commentService)
        {
            UserService = userService;
            PostService = postService;
            CommentService = commentService;
        }

        [HttpGet]
        [Authorize(Roles = "Owner")]
        public ActionResult<List<User>> Get() => UserService.Get();

        [HttpGet("{id:int}", Name = "GetUser")]
        [Authorize]
        public ActionResult<User> Get([FromRoute] int id)
        {
            var user = UserService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpGet]
        [Route("getUserName/{id:int}")]
        public ActionResult<string> GetUserName([FromRoute] int id)
        {
            var user = UserService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user.Name + (user.Surname != "" ? $" {user.Surname}" : string.Empty);
        }

        [HttpGet]
        [Authorize]
        [Route("getUserEmail/{id:int}")]
        public ActionResult<string> GetUserEmail([FromRoute] int id)
        {
            var user = UserService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user.Email;
        }

        [HttpPost]
        public ActionResult<User> Create([FromBody] User user)
        {
            if (user.Email.Equals("") || user.Name.Equals(""))
            {
                return StatusCode(401, "Uzupełnij wymagane pola");
            }
            if (UserService.Get(user.Email) != null)
            {
                return StatusCode(401, "Użytkownik został zbanowany :(");
            }

            string hexHash = GetHashedPassword(user.Password);

            var ids = UserService.Get().Select(e => e.UserId);
            var newId = Enumerable.Range(1, int.MaxValue)
                .Except(ids)
                .FirstOrDefault();

            user.UserId = newId;
            user.Password = hexHash;

            UserService.Create(user);

            return CreatedAtRoute("GetUser", new { id = user.UserId.ToString() }, user);
        }

        [HttpPut("{id:int}")]
        [Authorize(Roles = "Owner")]
        public IActionResult Update([FromRoute] int id, [FromBody] User userIn)
        {
            var user = UserService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            UserService.Update(id, userIn);

            if (userIn.IsBanned)
            {
                var comments = CommentService.GetByUserId(id);
                foreach (var comment in comments)
                {
                    CommentService.Remove(comment.CommentId);
                }
            }

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        [Authorize]
        public IActionResult Delete([FromRoute] int id)
        {
            var user = UserService.Get(id);

            if (user == null)
            {
                return NotFound();
            }
            else if (user.Role == (int)UserService.Role.Owner)
            {
                return Unauthorized();
            }
            else if (user.Role == (int)UserService.Role.Writer)
            {
                var writerPosts = PostService.GetByUserId(id);

                foreach (var post in writerPosts)
                {
                    post.UserId = 0;
                    PostService.Update(post.PostId, post);
                }
            }

            UserService.Remove(user.UserId);

            var comments = CommentService.GetByUserId(id);
            foreach (var comment in comments)
            {
                CommentService.Remove(comment.CommentId);
            }

            return NoContent();
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody] User user)
        {
            var hexHash = GetHashedPassword(user.Password);
            var userLoggedIn = UserService.Login(user.Email, hexHash);

            if (userLoggedIn != null)
            {
                if (!userLoggedIn.IsBanned)
                {
                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("A#2!Meg@W0nsZ9!2#A"));
                    var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                    var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, userLoggedIn.UserId.ToString()),
                    new Claim(ClaimTypes.Name, userLoggedIn.Name),
                    new Claim(ClaimTypes.Email, userLoggedIn.Email),
                    new Claim(ClaimTypes.Role, Enum.GetName(typeof(UserService.Role), userLoggedIn.Role)),
                };

                    var tokenOptions = new JwtSecurityToken(
                        issuer: "https://localhost:54388",
                        audience: "https://localhost:54388",
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(5),
                        signingCredentials: signingCredentials
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);

                    return Ok(new { Token = tokenString });
                }
                else
                {
                    return StatusCode(401, "Użytkownik został zbanowany :(");
                }

            }

            return StatusCode(401, "Niepoprawny email lub hasło");
        }

        private static string GetHashedPassword(string password)
        {
            SHA256 mySHA256 = SHA256.Create();
            var hash = new StringBuilder();
            var sha256Password = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(password));

            foreach (byte theByte in sha256Password)
            {
                hash.Append(theByte.ToString("x2"));
            }
            var hexHash = hash.ToString();
            return hexHash;
        }
    }
}