﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FloraBlog.Controllers
{
    [Route("api/upload")]
    public class UploadController : Controller
    {
        [HttpPost, DisableRequestSizeLimit]
        [Authorize(Roles = "Writer, Owner")]
        public IActionResult Upload(IFormFile image, [FromForm] string newFolderName)
        {
            try
            {
                var file = image;
                var folderReferenceName = Path.Combine("FloraBlog", "Posts");

                if (!Directory.Exists(Path.Combine(Path.GetTempPath(), "FloraBlog")))
                {
                    Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), "FloraBlog"));
                }

                if (!Directory.Exists(Path.Combine(Path.GetTempPath(), folderReferenceName)))
                {
                    Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), folderReferenceName));
                }

                var pathToSave = Path.Combine(Path.GetTempPath(), folderReferenceName, newFolderName);

                if (!Directory.Exists(pathToSave))
                {
                    Directory.CreateDirectory(pathToSave);
                }

                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    return Ok(new
                    {
                        uploaded = true,
                        url = fullPath
                    });
                }
                else
                {
                    return NotFound(new
                    {
                        uploaded = false,
                        error = "Could not upload this image"
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
    }
}