using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using FloraBlog.Models;
using System.Net;
using System.Net.Mail;

namespace FloraBlog.Controllers
{
    [Route("api/email")]
    public class EmailController : Controller
    {
        [HttpPost]
        public IActionResult EmailSend([FromBody] Email email)
        {
            var from = new MailAddress(email.EmailAddress);
            var to = new MailAddress("contact@frolablog.pl");

            var message = new MailMessage(from, to)
            {
                Subject = "Wiadomo�� z formularza kontaktowego Flora Blog",
                Body = email.Body,
            };

            var client = new SmtpClient("smtp.mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("8086418478a631", "fd70d1dc8fe911"),
                EnableSsl = true
            };

            if (client != null)
            {
                client.Send(message);
            }

            return NoContent();
        }
    }
}
