﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using FloraBlog.Models;
using FloraBlog.Services;
using Microsoft.AspNetCore.Authorization;

namespace FloraBlog.Controllers
{
    [Route("api/comments")]
    public class CommentController : Controller
    {
        private readonly CommentService CommentService;

        public CommentController(CommentService commentService)
        {
            CommentService = commentService;
        }

        [HttpGet]
        public ActionResult<List<Comment>> Get() => CommentService.Get();

        [HttpGet]
        [Route("getByPost/{id:int}")]
        public ActionResult<List<Comment>> GetByPostId([FromRoute] int id) 
            => CommentService.GetByPostId(id).Where(e => e.RefCommentId == 0).ToList();

        [HttpGet]
        [Route("getByRefComment/{id:int}")]
        public ActionResult<List<Comment>> GetByRefCommentId([FromRoute] int id) 
            => CommentService.GetByRefCommentId(id);

        [HttpGet("{id:int}", Name = "GetComment")]
        public ActionResult<Comment> Get([FromRoute] int id)
        {
            var Comment = CommentService.Get(id);

            if (Comment == null)
            {
                return NotFound();
            }

            return Comment;
        }

        [HttpPost]
        [Authorize]
        public ActionResult<Comment> Create([FromBody] Comment comment)
        {
            var ids = CommentService.Get().Select(e => e.CommentId);
            var newId = Enumerable.Range(1, int.MaxValue)
                .Except(ids)
                .FirstOrDefault();

            comment.CommentId = newId;
            CommentService.Create(comment);

            return CreatedAtRoute("GetComment", new { id = comment.CommentId.ToString() }, comment);
        }

        [HttpPut("{id:int}")]
        [Authorize(Roles = "Writer, Owner")]
        public IActionResult Update([FromRoute] int id, [FromBody] Comment commentIn)
        {
            var Comment = CommentService.Get(id);

            if (Comment == null)
            {
                return NotFound();
            }

            CommentService.Update(id, commentIn);

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        [Authorize]
        public IActionResult Delete([FromRoute] int id)
        {
            var Comment = CommentService.Get(id);

            if (Comment == null)
            {
                return NotFound();
            }

            CommentService.Remove(Comment.CommentId);

            return NoContent();
        }
    }
}