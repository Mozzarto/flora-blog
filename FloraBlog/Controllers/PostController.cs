﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FloraBlog.Models;
using FloraBlog.Services;

namespace FloraBlog.Controllers
{
    [Route("api/posts")]
    public class PostController : Controller
    {
        private readonly PostService PostService;
        private readonly CommentService CommentService;

        public PostController(PostService postService, CommentService commentService)
        {
            PostService = postService;
            CommentService = commentService;
        }

        [HttpGet]
        public ActionResult<List<Post>> Get() => PostService.Get();

        [HttpGet("{id:int}", Name = "GetPost")]
        public ActionResult<Post> Get([FromRoute] int id)
        {
            var post = PostService.Get(id);

            if (post == null)
            {
                return NotFound();
            }

            return post;
        }

        [HttpPost]
        [Authorize(Roles = "Writer, Owner")]
        public ActionResult<Post> Create([FromBody] Post post)
        {
            var ids = PostService.Get().Select(e => e.PostId);
            var newId = Enumerable.Range(1, int.MaxValue)
                .Except(ids)
                .FirstOrDefault();

            post.PostId = newId;

            DeleteUnnecessaryImages(post);
            PostService.Create(post);
            return CreatedAtRoute("GetPost", new { id = post.PostId.ToString() }, post);
        }

        [HttpPut("{id:int}")]
        [Authorize(Roles = "Writer, Owner")]
        public IActionResult Update([FromRoute] int id, [FromBody] Post postIn)
        {
            var post = PostService.Get(id);

            if (post == null)
            {
                return NotFound();
            }

            DeleteUnnecessaryImages(postIn);
            PostService.Update(id, postIn);
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        [Authorize(Roles = "Owner")]
        public IActionResult Delete([FromRoute] int id)
        {
            var post = PostService.Get(id);

            if (post == null)
            {
                return NotFound();
            }

            var destinationPath = Path.Combine("ClientApp", "src", "assets", "images", "posts", post.FolderName);
            if (Directory.Exists(destinationPath))
            {
                Directory.Delete(destinationPath);
            }

            PostService.Remove(post.PostId);

            var comments = CommentService.GetByPostId(id);
            foreach (var comment in comments)
            {
                CommentService.Remove(comment.CommentId);
            }

            return NoContent();
        }

        private void DeleteUnnecessaryImages(Post post)
        {
            var domainName = HttpContext.Request.Host.Value;
            var path = Path.Combine("ClientApp", "src", "assets", "images", "posts", post.FolderName);
            var clientSidePath = Path.Combine("http://", domainName, "assets", "images", "posts", post.FolderName);
            var tempPath = Path.Combine(Path.GetTempPath(), "FloraBlog");

            if (Directory.Exists(path))
            {
                var allFiles = Directory.GetFiles(path);

                foreach (var file in allFiles)
                {
                    string fileName = file.Substring(path.Length + 1);

                    if (!post.Text.Contains(Path.Combine(clientSidePath, fileName).Replace(@"\", @"/")))
                    {
                        System.IO.File.Delete(Path.Combine(path, fileName));
                    }
                }

                if (!Directory.GetFiles(path).Any())
                {
                    Directory.Delete(path);
                }
            }

            CopyImagesFromServerToClient(post, domainName, path, clientSidePath, tempPath);

            if (Directory.Exists(tempPath) && !Directory.GetFiles(tempPath, "*.*", SearchOption.AllDirectories).Any())
            {
                Directory.Delete(tempPath, true);
            }
        }

        private void CopyImagesFromServerToClient(
            Post post, 
            string domainName, 
            string destinationPath, 
            string clientSidePath,
            string tempPath)
        {
            var referencePath = Path.Combine(tempPath, "Posts", post.FolderName);

            if (Directory.Exists(referencePath))
            {
                var filesToCopy = Directory.GetFiles(referencePath);

                foreach (var fileToCopy in filesToCopy)
                {
                    string fileName = fileToCopy.Substring(referencePath.Length + 1);

                    if (!Directory.Exists(destinationPath))
                    {
                        Directory.CreateDirectory(destinationPath);
                    }

                    System.IO.File.Copy(Path.Combine(referencePath, fileName), Path.Combine(destinationPath, fileName), true);
                    System.IO.File.Delete(Path.Combine(referencePath, fileName));

                    var newURL = Path.Combine(clientSidePath, fileName).Replace(@"\", @"/");
                    post.Text = post.Text.Replace($"{Path.Combine(referencePath, fileName)}", $"{newURL}");
                }
            }
        }
    }
}