﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;
using FloraBlog.Models;

namespace FloraBlog.Services
{
    public class CommentService
    {
        private readonly IMongoCollection<Comment> Comments;

        public CommentService(IFloraDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            Comments = database.GetCollection<Comment>("comments");
        }

        public List<Comment> Get() => Comments.Find(Comment => true).ToList();

        public Comment Get(int id) => Comments.Find(Comment => Comment.CommentId == id).FirstOrDefault();

        public List<Comment> GetByUserId(int id) => Comments.Find(Comment => Comment.UserId == id).ToList();

        public List<Comment> GetByPostId(int id) => Comments.Find(Comment => Comment.PostId == id).ToList();

        public List<Comment> GetByRefCommentId(int id) => Comments.Find(Comment => Comment.RefCommentId == id).ToList();

        public Comment Create(Comment Comment)
        {
            Comments.InsertOne(Comment);
            return Comment;
        }

        public void Update(int id, Comment CommentIn) => Comments.ReplaceOne(Comment => Comment.CommentId == id, CommentIn);

        public void Remove(Comment CommentIn) => Comments.DeleteOne(Comment => Comment.CommentId == CommentIn.CommentId);

        public void Remove(int id) => Comments.DeleteOne(Comment => Comment.CommentId == id);
    }
}
