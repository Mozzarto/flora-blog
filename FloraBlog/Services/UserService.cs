﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;
using FloraBlog.Models;

namespace FloraBlog.Services
{
    public class UserService
    {
        private readonly IMongoCollection<User> Users;

        public UserService(IFloraDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            Users = database.GetCollection<User>("users");
        }

        public List<User> Get() => Users.Find(User => User.Role != (int)Role.Owner).ToList();

        public User Get(int id) => Users.Find(User => User.UserId == id).FirstOrDefault();

        public User Get(string email) => Users.Find(User => User.Email == email).FirstOrDefault();

        public User Create(User User)
        {
            Users.InsertOne(User);
            return User;
        }

        public void Update(int id, User UserIn) => Users.ReplaceOne(User => User.UserId == id, UserIn);

        public void Remove(User UserIn) => Users.DeleteOne(User => User.UserId == UserIn.UserId);

        public void Remove(int id) => Users.DeleteOne(User => User.UserId == id);

        public User Login(string email, string password)
        {
            var userWithEmail = Users.Find(User => User.Email == email).ToList();

            if (userWithEmail.Any())
            {
                return userWithEmail.Where(User => User.Password == password).FirstOrDefault();
            }

            return null;
        }

        public enum Role
        {
            User = 0,
            Writer = 1,
            Owner = 2
        }
    }
}
