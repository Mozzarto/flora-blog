﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;
using FloraBlog.Models;

namespace FloraBlog.Services
{
    public class PostService
    {
        private readonly IMongoCollection<Post> Posts;

        public PostService(IFloraDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            Posts = database.GetCollection<Post>("posts");
        }

        public List<Post> Get() => Posts.Find(Post => true).ToList();

        public Post Get(int id) => Posts.Find(Post => Post.PostId == id).FirstOrDefault();

        public List<Post> GetByUserId(int id) => Posts.Find(Post => Post.UserId == id).ToList();

        public Post Create(Post Post)
        {
            Posts.InsertOne(Post);
            return Post;
        }

        public void Update(int id, Post PostIn) => Posts.ReplaceOne(Post => Post.PostId == id, PostIn);

        public void Remove(Post PostIn) => Posts.DeleteOne(Post => Post.PostId == PostIn.PostId);

        public void Remove(int id) => Posts.DeleteOne(Post => Post.PostId == id);
    }
}
