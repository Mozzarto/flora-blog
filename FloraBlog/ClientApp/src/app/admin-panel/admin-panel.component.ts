import { Component, Input, OnInit } from '@angular/core';
import { PostService } from '../services/post.service'
import { CommentService } from '../services/comment.service'
import { UserService } from '../services/user.service'
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  @Input() postData: Array<any>;
  @Input() commentData: Array<any>;
  @Input() userData: Array<any>;
  public currentPost: any;

  selectedMenuElement: string;
  isStandardUser: boolean;
  menu: string[];

  constructor(
    private postService: PostService,
    private commentService: CommentService,
    private userService: UserService,
    private authService: AuthService
  ){
    postService.get().subscribe((data: any) => this.postData = data);
    commentService.get().subscribe((data: any) => this.commentData = data);
    if (this.authService.isAdminUser()) {
      userService.get().subscribe((data: any) => this.userData = data);
    }
    this.isStandardUser = this.authService.isStandardUser();
    this.currentPost = this.setInitialValuesForPostData();
    if (this.authService.isAdminUser()) {
      this.menu = ['Posty', 'Komentarze', 'Użytkownicy'];
    }
    else {
      this.menu = ['Posty', 'Komentarze'];
    }
  }

  ngOnInit(): void {
  }

  private setInitialValuesForPostData() {
    return {
      postId: undefined,
      date: new Date(),
      title: '',
      image: '',
      text: '',
    }
  }

  public createOrUpdatePostClicked = function (post: any) {
    let postWithId;
    postWithId = this.postData.find(x => x.postId === post.postId);

    if (postWithId) {
      const updateIndex = this.postData.findIndex(x => x.postId === postWithId.postId);
      this.postService.update(post).subscribe(
        result => this.postData.splice(updateIndex, 1, post)
      );
    }
    else {
      post.folderName = post.date.valueOf();
      post.date = post.date.toISOString();
      post.userId = this.authService.getUserId();

      this.postService.add(post).subscribe(
        result => {
          result.canDelete = this.authService.checkIfIsOwnerOrLoggedInUser(result);
          this.postData.push(result);
        }
      );
    }

    this.currentPost = this.setInitialValuesForPostData();
  };

  public editPostClicked = function (item) {
    this.currentPost = item;
  };

  public editOrBanUserClicked = function (user: any) {
    let userWithId;
    userWithId = this.userData.find(x => x.userId === user.userId);

    if (userWithId) {
      const updateIndex = this.userData.findIndex(x => x.userId === userWithId.userId);
      this.userService.update(user).subscribe(
        result => this.userData.splice(updateIndex, 1, user)
      );
    }
  };

  public newPostClicked = function () {
    this.currentPost = this.setInitialValuesForPostData();
  };

  public deletePostClicked(item) {
    const deleteIndex = this.postData.findIndex(x => x.postId === item.postId);
    this.postService.remove(item.postId).subscribe(
      result => this.postData.splice(deleteIndex, 1)
    );
  };

  public deleteCommentClicked(item) {
    const deleteIndex = this.commentData.findIndex(x => x.commentId === item.commentId);
    this.commentService.remove(item.commentId).subscribe(
      result => this.commentData.splice(deleteIndex, 1)
    );
  };

  public deleteUserClicked(item) {
    const deleteIndex = this.userData.findIndex(x => x.userId === item.userId);
    this.userService.remove(item.userId).subscribe(
      result => this.userData.splice(deleteIndex, 1)
    );
  };
}
