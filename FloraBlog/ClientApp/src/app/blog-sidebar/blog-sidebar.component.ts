import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { ActivatedRoute } from '@angular/router';
import { mergeMap, toArray, mergeAll, groupBy } from 'rxjs/operators';
import { of, from, zip } from 'rxjs';


@Component({
  selector: 'app-blog-sidebar',
  templateUrl: './blog-sidebar.component.html',
  styleUrls: ['./blog-sidebar.component.css']
})
export class BlogSidebarComponent implements OnInit {
  posts: any;
  postsArchive: Array<any>;
  postGroupedByDate: any = [];

  constructor(private postService: PostService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.postService.get().subscribe(
      (data: any) => {
        this.posts = data;
        this.postsArchive = data;
        this.posts.sort((a, b) => (a['date'] < b['date']) ? 1 : -1);

        from(this.postsArchive).pipe(
          groupBy(item => new Date(item.date).getFullYear()),
          mergeMap(group => zip(of(group.key), group.pipe(toArray()))))
          .subscribe(p => {
           var length = this.postGroupedByDate.push(p);
          });
      });
  }
}
