import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../services/user.service'
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input() userItem: any;
  invalidLogin: boolean;
  isLoggedIn: boolean;
  isStandardUser: boolean;
  errorMessage: any;

  constructor(
    private router: Router,
    private userService: UserService,
    private authService: AuthService,
    private jwtHelper: JwtHelperService)
  {
    if (this.userItem == null) {
      this.userItem = this.setDefaultUserData();
    }
    this.isLoggedIn = this.authService.isUserAuthenticated();
    this.isStandardUser = this.authService.isStandardUser();
    this.errorMessage == '';
  }

  ngOnInit(): void {
  }

  private setDefaultUserData() {
    return {
      email: '',
      password: '',
    }
  }

  public login() {
    this.userService.login(this.userItem).subscribe(response => {
      const token = (<any>response).token;
      localStorage.setItem("jwt", token);
      this.errorMessage = '';
      this.invalidLogin = false;
      this.router.navigate(["/"]);
    }, err => {
      this.errorMessage = err.error;
      this.invalidLogin = true;
    });
  };

  public deleteAccount() {
    const token: string = localStorage.getItem("jwt");

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      const userId = this.authService.getUserId();
      const userEmail = this.authService.getUserEmail();

      this.userService.getUserEmail(userId).subscribe(email => {
        if (userEmail === email) {
          this.userService.remove(userId).subscribe(response => {
            this.router.navigate(['/']);
          });
        }
      });
    }
  }

  public logout() {
    localStorage.removeItem("jwt");
    this.userItem = this.setDefaultUserData();

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/login']);
    }); 
  };
}
