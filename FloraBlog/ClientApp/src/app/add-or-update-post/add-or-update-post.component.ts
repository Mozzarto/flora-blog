import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { MyUploadAdapter } from '../services/my-upload-adapter';

@Component({
  selector: 'app-add-or-update-post',
  templateUrl: './add-or-update-post.component.html',
  styleUrls: ['./add-or-update-post.component.css']
})
export class AddOrUpdatePostComponent implements OnInit {
  @Output() createOrUpdatePostClicked = new EventEmitter<any>();
  @Input() postDataItem: any;

  public Editor = ClassicEditor;
  public buttonText = 'Zapisz';

  constructor() {
    this.clearpostDataItem();
  }

  ngOnInit() {
  }

  onReady(Editor: ClassicEditor): void {
    Editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
      return new MyUploadAdapter(loader, new Date(this.postDataItem.date).valueOf());
    };
  };

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  };

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.postDataItem.image = btoa(binaryString);
  };

  private clearpostDataItem = function () {
    this.postDataItem = {
      postId: undefined,
      date: new Date(),
      title: '',
      image: '',
      text: '',
    };
  };

  public createOrUpdatePostRecord = function (event) {
    this.postDataItem.date = new Date(this.postDataItem.date);
    this.createOrUpdatePostClicked.emit(this.postDataItem);
    this.clearpostDataItem();
  };
}
