import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrUpdatePostComponent } from './add-or-update-post.component';

describe('AddOrUpdatePostComponent', () => {
  let component: AddOrUpdatePostComponent;
  let fixture: ComponentFixture<AddOrUpdatePostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOrUpdatePostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOrUpdatePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
