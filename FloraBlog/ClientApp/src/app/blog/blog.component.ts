import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-blog-component',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css', '../ckeditor-style.css']
})
export class BlogComponent implements OnInit {
  @Input() postData: Array<any>;
  @Input() term: string;
  allPosts: Array<any>;
  config: any;

  constructor(private httpService: HttpClient) { }
  
  ngOnInit(): void {
    this.httpService.get('http://localhost:54388/api/posts').subscribe(
      data => {
        this.allPosts = data as Array<any>;
        this.allPosts.sort((a, b) => (a.date < b.date) ? 1 : -1);
        this.config = {
          itemsPerPage: 4,
          currentPage: 1,
          totalItems: this.allPosts.length
          };
        }
    );
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }
}
