import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-blog-carousel',
  templateUrl: './blog-carousel.component.html',
  styleUrls: ['./blog-carousel.component.css', '../ckeditor-style.css']
})
export class BlogCarouselComponent implements OnInit {
  postData: any;

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.postService.get().subscribe(
      (data: any) => {
        this.postData = data;
        this.postData.sort((a, b) => (a['date'] < b['date']) ? 1 : -1);
      });
  }
}
