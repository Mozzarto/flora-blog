import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private headers: HttpHeaders;
  private accessPointUrl: string = 'http://localhost:54388/api/users';
  currentUser: any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public get() {
    return this.http.get(this.accessPointUrl, { headers: this.headers });
  }

  public getUserName(userId) {
    return this.http.get(this.accessPointUrl + '/getUserName/' + userId, { responseType: 'text', headers: this.headers });
  }

  public getUserEmail(userId) {
    return this.http.get(this.accessPointUrl + '/getUserEmail/' + userId, { responseType: 'text', headers: this.headers });
  }

  public add(userItem) {
    return this.http.post(this.accessPointUrl, userItem, { headers: this.headers });
  }

  public remove(userId) {
    return this.http.delete(this.accessPointUrl + '/' + userId, { headers: this.headers });
  }

  public update(userItem) {
    return this.http.put(this.accessPointUrl + '/' + userItem.userId, userItem, { headers: this.headers });
  }

  public login(userItem) {
    return this.http.post(this.accessPointUrl + '/login', userItem, { headers: this.headers });
  }
}
