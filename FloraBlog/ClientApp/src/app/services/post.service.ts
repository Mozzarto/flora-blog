import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private headers: HttpHeaders;
  private accessPointUrl: string = 'http://localhost:54388/api/posts';

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public get() {
    return this.http.get(this.accessPointUrl, { headers: this.headers });
  }

  public getPost(postId) {
    return this.http.get(this.accessPointUrl + '/' + postId, { headers: this.headers });
  }

  public add(postItem) {
    return this.http.post(this.accessPointUrl, postItem, { headers: this.headers });
  }

  public remove(postId) {
    return this.http.delete(this.accessPointUrl + '/' + postId, { headers: this.headers });
  }

  public update(postItem) {
    return this.http.put(this.accessPointUrl + '/' + postItem.postId, postItem, { headers: this.headers });
  }
}
