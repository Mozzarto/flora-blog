import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private headers: HttpHeaders;
  private accessPointUrl: string = 'http://localhost:54388/api/email';

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public sendMail(emailData) {
    return this.http.post(this.accessPointUrl, emailData, { headers: this.headers });
  }
}
