import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private jwtHelper: JwtHelperService) {
  }

  isUserAuthenticated() {
    const token: string = localStorage.getItem("jwt");

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    else {
      return false;
    }
  }

  isStandardUser() {
    const token: string = localStorage.getItem("jwt");

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      const decodedToken = this.jwtHelper.decodeToken(token);

      if ('User' === decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']) {
        return true;
      }

      return false;
    }
  }

  isAdminUser() {
    const token: string = localStorage.getItem("jwt");

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      const decodedToken = this.jwtHelper.decodeToken(token);

      if ('Owner' === decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']) {
        return true;
      }

      return false;
    }
  }

  checkIfIsOwnerOrLoggedInUser(postData) {
    if (this.isAdminUser() || (postData.userId != '' && postData.userId == this.getUserId())) {
      return true;
    }
    else {
      return false;
    }
  }

  getUserId() {
    const token: string = localStorage.getItem("jwt");
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier'];
  }

  getUserEmail() {
    const token: string = localStorage.getItem("jwt");
    const decodedToken = this.jwtHelper.decodeToken(token);
    return decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'];
  }
}
