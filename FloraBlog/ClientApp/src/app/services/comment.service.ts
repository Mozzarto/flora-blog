import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private headers: HttpHeaders;
  private accessPointUrl: string = 'http://localhost:54388/api/comments';

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
  }

  public get() {
    return this.http.get(this.accessPointUrl, { headers: this.headers });
  }

  public getByPostId(postId) {
    return this.http.get(this.accessPointUrl + '/getByPost/' + postId, { headers: this.headers });
  }

  public getByRefCommentId(commentId) {
    return this.http.get(this.accessPointUrl + '/getByRefComment/' + commentId, { headers: this.headers });
  }

  public add(commentItem) {
    return this.http.post(this.accessPointUrl, commentItem, { headers: this.headers });
  }

  public remove(commentId) {
    return this.http.delete(this.accessPointUrl + '/' + commentId, { headers: this.headers });
  }
}
