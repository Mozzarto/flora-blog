import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  public route: string;
  constructor(private router: Router) { }

  ngOnInit() {
    this.route = this.router.url;
  }
}

