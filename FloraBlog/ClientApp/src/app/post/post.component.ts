import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PostService } from '../services/post.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css', '../ckeditor-style.css']
})
export class PostComponent implements OnInit {
  postId: string;
  singlePost: any;
  public Editor = ClassicEditor;

  constructor(private route: ActivatedRoute, private postService: PostService) {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['id'];
      this.postService.getPost(this.postId).subscribe((data: any) => this.singlePost = data);
    });}

  ngOnInit(): void {
  }
}
