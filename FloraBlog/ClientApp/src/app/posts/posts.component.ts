import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  @Output() deletePostClicked = new EventEmitter<any>();
  @Output() newPostClicked = new EventEmitter<any>();
  @Output() editPostClicked = new EventEmitter<any>();
  @Input() postData: Array<any>;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    if (this.postData) {
      for (var i = 0; i < this.postData.length; i++) {
        this.postData[i].canDelete = this.authService.checkIfIsOwnerOrLoggedInUser(this.postData[i]);
      }
    }
  }

  public deletePostRecord(record) {
    this.deletePostClicked.emit(record);
  }

  public editPostRecord(record) {
    const clonedRecord = Object.assign({}, record);
    this.editPostClicked.emit(clonedRecord);
  }

  public newPostRecord() {
    this.newPostClicked.emit();
  }
}
