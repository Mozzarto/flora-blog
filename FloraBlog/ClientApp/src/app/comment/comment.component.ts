import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { CommentService } from '../services/comment.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  postId: any;
  commentData: Array<any>;
  amountOfComments: any;
  isLoggedIn: boolean;
  commentText: any;
  refCommentId: any;
  replyInfo: any;

  constructor(
    private route: ActivatedRoute, 
    private commentService: CommentService,
    private userService: UserService,
    private authService: AuthService
  ) {
    this.isLoggedIn = this.authService.isUserAuthenticated();
    this.refCommentId = 0;
    this.amountOfComments = 0;

    this.route.params.subscribe((params: Params) => {
      this.postId = params['id'];
      this.commentService.getByPostId(this.postId).subscribe((data: any) => {
        this.commentData = data;
        if (this.commentData) {
          this.amountOfComments = this.commentData.length;

          for (const comment of this.commentData) {
            this.userService.getUserName(comment.userId).subscribe(data => comment.userName = data);
            this.commentService.getByRefCommentId(comment.commentId).subscribe((data: any) => {
              comment.refComments = data;
              comment.refComments.sort((a, b) => (a['date'] < b['date']) ? 1 : -1);

              for (const refComment of comment.refComments) {
                this.userService.getUserName(refComment.userId).subscribe(data => refComment.userName = data);
              }

              this.amountOfComments = this.amountOfComments + comment.refComments.length;
            });
          }
          this.commentData.sort((a, b) => (a['date'] < b['date']) ? 1 : -1);
        }
      });
    });
  }

  ngOnInit(): void {
  }

  addComment() {
    var commentItem = {
      postId: this.postId,
      date: new Date(),
      userId: this.authService.getUserId(),
      commentText: this.commentText,
      refCommentId: this.refCommentId,
    };

    this.commentService.add(commentItem).subscribe(
      result => {
        this.userService.getUserName(result['userId']).subscribe(data => result['userName'] = data);
        if (this.refCommentId != 0) {
          var refCommentIndex = this.commentData.findIndex(element => element.commentId == this.refCommentId);
          this.commentData[refCommentIndex].refComments.push(result);
          this.commentData[refCommentIndex].refComments.sort((a, b) => (a['date'] < b['date']) ? 1 : -1);
        }
        else {
          this.commentData.push(result);
          this.commentData.sort((a, b) => (a['date'] < b['date']) ? 1 : -1);
        }
        this.refCommentId = 0;
        this.commentText = "";
        this.amountOfComments = this.amountOfComments + 1;
      });
  }

  replyTo(commentId, commentInfo) {
    this.refCommentId = commentId;
    if (this.refCommentId != 0) {
      this.replyInfo = "Odpowiedasz na: " + commentInfo.commentText + " ~" + commentInfo.userName;
    }
    else {
      this.replyInfo = "";
    }
  }
}
