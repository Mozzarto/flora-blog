import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { PostService } from '../services/post.service'
import { UserService } from '../services/user.service'

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Output() deleteCommentClicked = new EventEmitter<any>();
  @Input() commentData: Array<any>;

  constructor(private postService: PostService, private userService: UserService) {}

  ngOnInit(): void {
    for (const comment of this.commentData) {
      this.postService.getPost(comment.postId).subscribe(data => comment.postMiniature = data['image']);
      this.userService.getUserName(comment.userId).subscribe(data => { comment.userName = data; });
    } 
  }

  public deleteCommentRecord(record) {
    this.deleteCommentClicked.emit(record);
  }
}
