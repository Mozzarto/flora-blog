import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Output() deleteUserClicked = new EventEmitter<any>();
  @Output() editOrBanUserClicked = new EventEmitter<any>();
  @Input() userData: Array<any>;

  constructor() { }

  ngOnInit(): void {
  }

  public deleteUserRecord(record) {
    this.deleteUserClicked.emit(record);
  }

  public editUserRecord(record) {
    const clonedRecord = Object.assign({}, record);
    this.editOrBanUserClicked.emit(clonedRecord);
  }

  public banUser(record) {
    const clonedRecord = Object.assign({}, record);

    if (clonedRecord.isBanned) {
      clonedRecord.isBanned = false;
    }
    else {
      clonedRecord.isBanned = true;
    }

    this.editOrBanUserClicked.emit(clonedRecord);
  }
}
