import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, ExtraOptions, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { JwtModule } from '@auth0/angular-jwt';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { DecimalPipe } from '@angular/common';
import { DatePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { BlogComponent } from './blog/blog.component';
import { BlogCarouselComponent } from './blog-carousel/blog-carousel.component';
import { BlogSidebarComponent } from './blog-sidebar/blog-sidebar.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { CommentComponent } from './comment/comment.component';
import { CommentsComponent } from './comments/comments.component';
import { UsersComponent } from './users/users.component';
import { PostComponent } from './post/post.component';
import { PostsComponent } from './posts/posts.component';
import { AddOrUpdatePostComponent } from './add-or-update-post/add-or-update-post.component';
import { ContactComponent } from './contact/contact.component';

import { PostService } from './services/post.service';
import { CommentService } from './services/comment.service';
import { UserService } from './services/user.service';
import { ContactService } from './services/contact.service';
import { AuthService } from './services/auth.service';

import { AuthGuard } from './guards/auth-guard.service';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'blog', component: BlogComponent },
  { path: 'post/:id', component: PostComponent },
  { path: 'admin-panel', component: AdminPanelComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];

const routingOptions: ExtraOptions = {
  anchorScrolling: 'enabled',
  scrollPositionRestoration: 'enabled',
  onSameUrlNavigation: 'reload',
};

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    BlogComponent,
    LoginComponent,
    AdminPanelComponent,
    PostsComponent,
    AddOrUpdatePostComponent,
    ContactComponent,
    CommentsComponent,
    UsersComponent,
    BlogCarouselComponent,
    PostComponent,
    BlogSidebarComponent,
    RegisterComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, routingOptions),
    NoopAnimationsModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatMenuModule,
    MatIconModule,
    Ng2SearchPipeModule,
    CKEditorModule,
    NgxPaginationModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:54388"],
        disallowedRoutes: []
      }
    })
  ],
  providers: [
    PostService,
    CommentService,
    UserService,
    ContactService,
    AuthService,
    AuthGuard,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

export function tokenGetter() {
  return localStorage.getItem("jwt");
}
