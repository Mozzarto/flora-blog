import { Component, OnInit, Input } from '@angular/core';
import { ContactService } from '../services/contact.service'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  @Input() emailData: any;

  constructor(private kontaktService: ContactService) {
    this.clearEmailData();
  }

  ngOnInit(): void {}

  private clearEmailData = function () {
    this.emailData = {
      name: '',
      emailAddress: '',
      body: '',
    };
  };


  public processForm = function() {
    this.kontaktService.sendMail(this.emailData).subscribe(
      result => this.clearEmailData()
    );
  }
}
