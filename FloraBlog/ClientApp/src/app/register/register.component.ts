import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Input() userItem: any;
  errorMessage: any;

  constructor(private router: Router, private userService: UserService) {
    if (this.userItem == null) {
      this.userItem = this.setDefaultUserData();
    }
    this.errorMessage = '';
  }

  ngOnInit(): void {
  }

  private setDefaultUserData() {
    return {
      email: '',
      name: '',
      surname: '',
      password: '',
    }
  }

  public register() {
    this.userService.add(this.userItem).subscribe(response => {
      this.errorMessage = '';
      this.router.navigate(["/"]);
    }, err => {
      this.errorMessage = err.error;
    });
  };

}
