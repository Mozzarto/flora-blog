﻿using System;

namespace FloraBlog.Models
{
    public class FloraDatabaseSettings : IFloraDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IFloraDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
