﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FloraBlog.Models
{
    [BsonIgnoreExtraElements]
    public class User
    {
        [BsonRequired]
        [BsonElement("userId")]
        public int UserId { get; set; }

        [BsonRequired]
        [BsonElement("email")]
        public string Email { get; set; }

        [BsonRequired]
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("surname")]
        public string Surname { get; set; }

        [BsonRequired]
        [BsonElement("password")]
        public string Password { get; set; }

        [BsonRequired]
        [BsonElement("role")]
        public int Role { get; set; } = 0;

        [BsonRequired]
        [BsonElement("isBanned")]
        public bool IsBanned { get; set; } = false;
    }
}
