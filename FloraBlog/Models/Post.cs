﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FloraBlog.Models
{
    [BsonIgnoreExtraElements]
    public class Post
    {
        [BsonRequired]
        [BsonElement("postId")]
        public int PostId { get; set; }

        [BsonRequired]
        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonRequired]
        [BsonElement("title")]
        public string Title { get; set; }

        [BsonRequired]
        [BsonElement("image")]
        public byte[] Image { get; set; }

        [BsonRequired]
        [BsonElement("text")]
        public string Text { get; set; }

        [BsonRequired]
        [BsonElement("folderName")]
        public string FolderName { get; set; }

        [BsonRequired]
        [BsonElement("userId")]
        public int UserId { get; set; }
    }
}
