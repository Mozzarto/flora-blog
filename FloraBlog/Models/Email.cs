using System;
using System.Net.Mail;

namespace FloraBlog.Models
{ 
	public class Email
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string Body { get; set; }
    }
}