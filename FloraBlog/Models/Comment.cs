﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FloraBlog.Models
{
    [BsonIgnoreExtraElements]
    public class Comment
    {
        [BsonRequired]
        [BsonElement("commentId")]
        public int CommentId { get; set; }

        [BsonRequired]
        [BsonElement("userId")]
        public int UserId { get; set; }

        [BsonRequired]
        [BsonElement("postId")]
        public int PostId { get; set; }

        [BsonRequired]
        [BsonElement("comment")]
        public string CommentText { get; set; }

        [BsonRequired]
        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonRequired]
        [BsonElement("refCommentId")]
        public int RefCommentId { get; set; } = 0;
    }
}
